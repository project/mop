<?php

/**
 * @file
 * Install, uninstall and update hooks for DPP.
 */

use Drupal\Core\Config\FileStorage;

/**
 * Implements hook_install().
 *
 * Performs actions to set up the site for this profile.
 *
 * @see system_install()
 *
 * @throws \Drupal\Core\Extension\MissingDependencyException
 */
function mop_install() {
  $default = 'dxpr_theme';
  $admin = 'gin';
  \Drupal::service('theme_installer')->install([$default, $admin]);
  \Drupal::configFactory()
    ->getEditable('system.theme')
    ->set('default', $default)
    ->set('admin', $admin)
    ->save();
}

/**
 * Configs update helper function.
 */
function _mop_apply_update($type, $name, $path, $ymls) {
  $theme_path = sprintf("%s/%s/", \Drupal::service('extension.list.' . $type)->getPath($name), $path);
  $config_factory = \Drupal::configFactory();
  $config_storage = \Drupal::service('config.storage');
  $configs = [];
  foreach ($ymls as $yml) {
    $configs[$yml] = $theme_path;
  }
  foreach ($configs as $config => $config_path) {
    $source = new FileStorage($config_path);
    $data = $source->read($config);
    if (is_array($data)) {
      $config_factory->getEditable($config)->setData($data)->save(TRUE);
      $config_storage->write($config, $data);
    }
    else {
      \Drupal::messenger()->addWarning(t('Incorrect data of @config', ['@config' => $config]));
    }
  }
}

/**
 * Configs remove helper function.
 */
function _mop_apply_delete($configs) {
  $config_factory = \Drupal::configFactory();
  /** @var \Drupal\Core\Config\StorageInterface $config_storage */
  $config_storage = \Drupal::service('config.storage');
  foreach ($configs as $config) {
    $config_factory->getEditable($config)->delete();
    $config_storage->delete($config);
  }
}

/**
 * Module disable helper function.
 */
function _mop_update_module_disable($modules) {
  $module_data = \Drupal::config('core.extension')->get('module');
  foreach ($modules as $item) {
    \Drupal::database()->delete('key_value')
      ->condition('collection', 'system.schema')
      ->condition('name', $item)
      ->execute();
    unset($module_data[$item]);
  }
  \Drupal::configFactory()
    ->getEditable('core.extension')
    ->set('module', $module_data)
    ->save();
  \Drupal::service('cache.config')->invalidateAll();
  foreach ($modules as $item) {
    if (\Drupal::moduleHandler()->moduleExists($item)) {
      \Drupal::service('module_installer')->uninstall([$item]);
    }
  }
  drupal_flush_all_caches();
}
