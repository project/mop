<?php

/**
 * @file
 * Enables modules and site configuration for a mop site installation.
 */

/**
 * Implements hook_preprocess_template().
 */
function mop_preprocess_install_page(&$variables) {
  $variables['site_version'] = '1.0';
}

/**
 * Implements hook_install_tasks().
 */
function mop_install_tasks(&$install_state) {
  $tasks = [
    'mop_clear_cache' => [
      'display_name' => t('Clear cache'),
      'display' => TRUE,
      'type' => 'normal',
      'function' => 'mop_clear_cache',
    ],
  ];
  return $tasks;
}

/**
 * Clear cache on installation.
 */
function mop_clear_cache() {
  drupal_flush_all_caches();
}
